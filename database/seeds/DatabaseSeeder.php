<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('insert into rooms values (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?)',
            [1, 'Просторный и светлый номер, предусматривающий размещение 8 человек.', 'Bright and spacious, the room provide accommodation for eight persons.', 180, 2, null, null,
                2, 'Милый и уютный номер, предусматривающий размещение для компании 4-х девушек.', 'Lovely & cozy room offers accommodation for four female guests.', 240, 2, null, null,
                3, 'Уютный номер для проживания 2-х гостей.', 'Homely room for two persons.', 500, 3, null, null,
                4, 'Уютный номер для проживания 2-х гостей.', 'Homely room for two persons.', 500, 3, null, null,
                5, 'Светлый номер рассчитан на 4-х гостей.', 'The sunny room can accommodate four guests.', 220, 3, null, null,
                6, 'Большой и просторный номер. Вмещает 10 гостей.', 'Large & spacious room for ten people.', 160, 3, null, null,
                7, 'Просторный и светлый номер. Вмещает 8 гостей.', 'The vast & bright room provides accommodation for eight guests.', 180, 2, null, null,
                8, 'Номер повышенной комфортности с телевизором, персональной уборной и душевой.', 'Superior room comes with TV, privet toilet and shower. ', 700, 3, null, null,
                9, 'Приятный и комфортный номер рассчитан на 6 человек.', 'Nice & comfortable room for 6 persons.', 200, 2, null, null]);

        /*DB::insert('insert into comments values (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?), (?, ?, ?, ?, ?)',
            [1, 'Анна Петрова', '2018-06-27', 2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.',
                2, 'Анна Петрова', '2018-06-26', 5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.',
                3, 'Анна Петрова', '2018-06-23', 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.',
                4, 'Анна Петрова', '2018-06-24', 4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.',
                5, 'Анна Петрова', '2018-06-21', 5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.',]);
    */
    }
}
