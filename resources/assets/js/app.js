/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(document).ready(function () {
    $("#to").prop('disabled', true);
    $('#from').datepicker({
        minDate: new Date(),
        autoClose: true,
        onSelect: function (dateText) {
            $("#to").prop('disabled', false).datepicker({
                minDate: new Date(dateText.split('.')[2], dateText.split('.')[1] - 1, 1 + +dateText.split('.')[0]),
                autoClose: true,
            });
        }
    });

    let locale = $('html').attr('lang');
    let err = locale === 'ru' ? 'Ошибка!' : 'Error!';
    let proces = locale === 'ru' ? 'обработка' : 'processing';
    let submit = locale === 'ru' ? 'Отправить' : 'Submit';

    $('#booking').submit(function (event) {
        event.preventDefault();
    //    let openWindow = window.open('/wait', '_blank');
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#bookingBtn').html(`<i class="fa fa-circle-o-notch fa-spin"></i> ${proces}`).prop('disabled', true);
            },
            success: function (res) {
                $('#bookingBtn').html(res.msg).prop('disabled', true);
                $('#booking input').val('');
                $('#booking select:first-child').prop('selected', 'selected');
            },
            error: function (err) {
                $('#bookingBtn').html(err).prop('disabled', true);
            },
            complete: function () {
                setTimeout(() => {
                    $('#bookingBtn').html(submit).prop('disabled', false);
                }, 3000);
                $('#booking input, #booking textarea').val("");
            }
        });
    });

    $('#phone').mask("+38 (999) 999-9999");
    $('.phone').mask("+38 (999) 999-9999");

    $('#feedback').submit(function (event) {
        event.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: new FormData(this),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#load').html(`<i class="fa fa-circle-o-notch fa-spin"></i> ${proces}`).prop('disabled', true);
            },
            success: function (result) {
                $('#load').html(result.msg).prop('disabled', true);
            },
            error: function () {
                $('#load').html(err).prop('disabled', true);
            },
            complete: function () {
                setTimeout(() => {
                    $('#load').html(submit).prop('disabled', false);
                }, 3000);
                $('#feedback input, #feedback textarea').val("");
            }
        });
    });

    function toggleMessage(data) {
        $('#bookingAlert').fadeIn().html(data);
        setTimeout(() => {
            $('#bookingAlert').fadeOut()
        }, 3000);
    }

    $('#video-btn').on('click', function () {
        $('#main-video').css('margin-top', 0);
        $('#frame-video').attr('src', 'video/hostel.mp4');
        $('#close-video').fadeIn();
    });

    $('#close-video').on('click', function () {
        $('#close-video').fadeOut();
        $('#frame-video').attr('src', '');
        $('#main-video').css('margin-top', '-200vh');
    });

    $('#comments-slider').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ],
        prevArrow: $('#prev-review'),
        nextArrow: $('#next-review'),
    });

    $('#rooms-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('.prev-slide-hotel'),
        nextArrow: $('.next-slide-hotel'),
    });
    $('.image-gallery').lightSlider({
        gallery: true,
        item: 1,
        thumbItem: 3,
        slideMargin: 0,
        speed: 300,
        auto: true,
        controls: false,
        loop: true,
        onSliderLoad: function () {
            $('.image-gallery').removeClass('cS-hidden');
        },
    });

    $('.navbar-collapse a').click(function () {
        $(".navbar-collapse").collapse('hide');
    });
});