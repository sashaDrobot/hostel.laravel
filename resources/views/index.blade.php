<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Hostel Station</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/datepicker.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/lightslider.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}"/>
</head>
<body>
<nav class="navbar navbar-light navbar-expand-xl fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/"><img src="img/logo.png" alt="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#home">{{ __('menu.home') }} <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#about">{{ __('menu.about') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#advantages">{{ __('menu.advantages') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#prices">{{ __('menu.prices') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#services">{{ __('menu.services') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contacts">{{ __('menu.contacts') }}</a>
                </li>
                <li class="nav-item d-flex">
                    <a href="#contacts" id="callback" class="main-button callback-btn">
                        {{ __('menu.callback') }}
                    </a>
                </li>
                <li class="nav-item d-flex" id="languages">
                    <a href="setlocale/ru"><span class="d-flex justify-content-center align-items-center">RU</span></a>
                    <a href="setlocale/en"><span class="d-flex justify-content-center align-items-center">EN</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<a href="#home" class="main-button booking-btn" id="cta">
    {{ __('booking.booking') }}
</a>

<div id="main-video" class="d-flex align-items-center justify-content-center"
     style="margin-top: -200vh; transition: margin-top .9s linear">
    <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" id="frame-video" src="" allowfullscreen></iframe>
        <i class="fa fa-times" id="close-video" aria-hidden="true"></i>
    </div>
</div>

<div class="container-fluid main-container" id="home">
    <div class="row" id="main-slider">
        <div id="main-slider-list">
            <div class="main-slider-inner"></div>
            <div class="main-slider-inner"></div>
            <div class="main-slider-inner"></div>
            <div class="main-slider-inner"></div>
            <div class="main-slider-inner"></div>
            <div class="main-slider-inner"></div>
            <div class="main-slider-inner"></div>
            <div class="main-slider-inner"></div>
            <div class="main-slider-inner"></div>
            <div class="main-slider-inner"></div>
            <div class="main-slider-inner"></div>
        </div>
    </div>
    <div class="container">
        <div class="row booking-form-row">
            <div class="col-lg-5 booking-form d-flex align-items-start align-self-center">
                <div class="booking-form-inner align-self-center">
                    <form id="booking" class="form d-flex flex-column" target="_blank" action="/booking" method="get">
                        @csrf
                        <div class="row">
                            <div class="form-group col-6">
                                <input name="name" class="form-control" autocomplete="off" type="text"
                                       placeholder="{{ __('booking.name') }}" required/>
                            </div>
                            <div class="form-group  col-6">
                                <input name="email" class="form-control" autocomplete="off" type="email"
                                       placeholder="{{ __('booking.email') }}" required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <input name="phone" class="form-control phone" autocomplete="off" type="tel"
                                       placeholder="{{ __('booking.phone') }}" required/>
                            </div>
                            <div class="form-group  col-6">
                                <input name="arrival" class="form-control" autocomplete="off" type="text"
                                       placeholder="{{ __('booking.arrival-date') }}" id="from"
                                       data-position="bottom right" readonly required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <select name="guests" class="form-control" required>
                                    <option value="" selected disabled>{{ __('booking.guests') }}</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div class="form-group col-6">
                                <input name="departure" class="form-control" autocomplete="off" type="text"
                                       placeholder="{{ __('booking.date-departure') }}" id="to"
                                       data-position="bottom right" disabled readonly required/>
                            </div>
                        </div>
                        <div class="alert" id="bookingAlert" role="alert">

                        </div>
                        <button type="submit" id="bookingBtn" class="main-button align-self-center">{{ __('booking.booking') }}</button>
                    </form>
                </div>
            </div>
            <div id="slider-control" class="col-1 offset-6">
                <div class="slider-control-inner d-flex align-items-center justify-content-center flex-column">
                    <span id="max-slide"></span>
                    <span class="vl"></span>
                    <span id="active-slide"></span>
                    <i class="fas fa-angle-right" aria-hidden="true" id="next-slide"></i>
                    <i class="fas fa-angle-left" aria-hidden="true" id="prev-slide"></i>
                </div>
            </div>
        </div>
        <div class="icons">
            <a href="https://instagram.com/stationhostelkh?utm_source=ig_profile_share&igshid=sbt9912alyd3" target="_blank">
                <img src="img/insta.png">
            </a>
            <a href="https://facebook.com/stationhostelkh/" target="_blank">
                <img src="img/fb.png">
            </a>
            <a href="https://www.tripadvisor.ru/Hotel_Review-g295369-d14759993-Reviews-Station_Hostel-Kharkiv_Kharkiv_Oblast.html" target="_blank">
                <i class="fab fa-tripadvisor"></i>
            </a>
        </div>
        <div class="stairs-wrapper d-flex justify-content-center">
            <div class="stairs" id="start-btn">
                <img src="img/icon.png">
            </div>
        </div>
    </div>
</div>

<div class="container-fluid about-container mt-5" id="about">
    <div class="row container-header">
        <div class="col-12 text-center">
            <h2>{{ __('menu.about') }}</h2>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p><strong>Station Hostel</strong>{{ __('about.about') }}</p>
            </div>
        </div>
        <div class="d-flex justify-content-center my-5">
            <div id="video-btn" class="d-flex justify-content-center align-items-center">
                <i class="fa fa-play" aria-hidden="true"></i>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid advantages-container" id="advantages">
    <div class="row container-header">
        <div class="col-12 text-center">
            <h2>{{__('advantages.title')}}<strong>Station Hostel</strong></h2>
        </div>
    </div>
    <div class="container">
        <div class="row adv-row justify-content-between">
            <div class="col-lg-3 adv-elem-wrapper d-flex align-items-center justify-content-between flex-column">
                <img src="{{ asset('img/train.png') }}" alt="Вокзал">
                <h6 class="text-center">{{ __('advantages.two-min') }}<br>{{ __('advantages.south-station') }}<br></h6>
            </div>
            <div class="col-lg-3 adv-elem-wrapper d-flex align-items-center justify-content-between flex-column">
                <img src="{{ asset('img/build.png') }}" alt="Город">
                <h6 class="text-center">{{ __('advantages.center') }} <br> {{ __('advantages.city') }}</h6>
            </div>
            <div class="col-lg-3 adv-elem-wrapper d-flex align-items-center justify-content-between flex-column">
                <img src="{{ asset('img/premium.svg') }}" alt="Премиум">
                <h6 class="text-center">{{ __('advantages.hostel') }}<br/>{{ __('advantages.premium') }}<br/></h6>
            </div>
            <div class="col-lg-3 adv-elem-wrapper d-flex align-items-center justify-content-between flex-column">
                <img src="{{ asset('img/guests.png') }}" alt="Персонал">
                <h6 class="text-center">{{ __('advantages.friendly') }}<br/>{{ __('advantages.staff') }}<br/></h6>
            </div>
            <div class="w-100 d-flex flex-column flex-lg-row justify-content-around">
                <div class="col-lg-3 adv-elem-wrapper d-flex align-items-center justify-content-between flex-column">
                    <img src="{{ asset('img/bathroom.png') }}" alt="">
                    <h6 class="text-center">{{ __('advantages.bathroom') }}<br/><br/></h6>
                </div>
                <div class="col-lg-3 adv-elem-wrapper d-flex align-items-center justify-content-between flex-column">
                    <img src="{{ asset('img/satellite_tv.png') }}" alt="">
                    <h6 class="text-center">{{ __('advantages.satellite') }}<br/><br/></h6>
                </div>
                <div class="col-lg-3 adv-elem-wrapper d-flex align-items-center justify-content-between flex-column">
                    <img src="{{ asset('img/breakfast.png') }}" alt="">
                    <h6 class="text-center">{{ __('advantages.kitchen') }}<br/><br/></h6>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid price-container" id="prices">
    <div class="row container-header mb-4">
        <div class="col-12 text-center" >
            <h2>{{ __('rooms.title') }}</h2>
        </div>
    </div>
    <div class="container">
        <div id="rooms-slider">
            @foreach($rooms as $room)
                <div>
                    <div class="row">
                        <div class="col-lg-7 hotel-photo-slider">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-2 left-arrow d-flex align-items-center justify-content-center">
                                    <div class="left-arrow-circle text-center prev-slide-hotel">
                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="col-lg-10">
                                    <div class="clearfix" style="max-width:100%">
                                        <ul class="gallery list-unstyled cS-hidden image-gallery">
                                            @for($i = 0; $i < $room->photos; $i++)
                                                <li data-thumb="img/room{{ $room->id }}/img{{ $i + 1 }}.jpg"
                                                    class="text-center">
                                                    <img src="img/room{{ $room->id }}/img{{ $i + 1 }}.jpg"
                                                         style="height: 371px"/>
                                                </li>
                                            @endfor
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 hotel-description-wrapper d-flex justify-content-between flex-column">
                            <div class="row">
                                <div class="col-12">
                                    <h3 class="hotel-header">{{ __('rooms.room') }} №{{ $room->id }}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <p>
                                        {{ app()->getLocale() === 'ru' ? $room->description_ru : $room->description_en }}
                                    </p>
                                </div>
                            </div>
                            <div class="row convenience-row">
                                <div class="col-6 d-flex align-items-center">
                                    <img src="img/towels.svg">
                                    <span class="convenience-description">{{ __('rooms.bedclothes') }}</span>
                                </div>
                                <div class="col-6 d-flex align-items-center px-0">
                                    <img src="img/matrass.svg">
                                    <span class="convenience-description" style="width: 50px">{{ __('rooms.mattresses') }}</span>
                                </div>
                            </div>
                            @if($room->id === 3 || $room->id ===4)
                                <div class="row convenience-row">
                                    <div class="col-6 d-flex align-items-center">
                                        <img src="img/nightstands.svg">
                                        <span class="convenience-description">{{ __('rooms.bedside ') }}</span>
                                    </div>

                                    <div class="col-6 d-flex align-items-center px-0">
                                        <img src="img/towel.png">
                                        <span class="convenience-description">{{ __('rooms.towel') }}</span>
                                    </div>
                                </div>
                                <div class="row convenience-row">
                                    <div class="col-6 d-flex align-items-center">
                                        <img src="img/lamp.svg">
                                        <span class="convenience-description">{{ __('rooms.lamp') }}</span>
                                    </div>
                                    <div class="col-6 d-flex align-items-center px-0">
                                        <img src="img/power_socket.svg">
                                        <span class="convenience-description">{{ __('rooms.power') }}</span>
                                    </div>
                                </div>
                            @else
                                <div class="row convenience-row">
                                    <div class="col-6 d-flex align-items-center">
                                        <img src="img/lamp.svg">
                                        <span class="convenience-description">{{ __('rooms.lamp') }}</span>
                                    </div>
                                    <div class="col-6 d-flex align-items-center px-0">
                                        <img src="img/power_socket.svg">
                                        <span class="convenience-description">{{ __('rooms.power') }}</span>
                                    </div>
                                </div>
                                @if($room->id === 8)
                                    <div class="row convenience-row">
                                        <div class="col-6 d-flex align-items-center">
                                            <img src="img/satellite.svg">
                                            <span class="convenience-description">{{ __('rooms.satellite') }}</span>
                                        </div>
                                        <div class="col-6 d-flex align-items-center px-0">
                                            <img src="{{ asset('img/towel.png') }}">
                                            <span class="convenience-description">{{ __('rooms.towel') }}</span>
                                        </div>
                                    </div>
                                @else
                                    <div class="row convenience-row">
                                        <div class="col-6 d-flex align-items-center">
                                            <img src="img/phone_stand.svg">
                                            <span class="convenience-description">{{ __('rooms.stand') }}</span>
                                        </div>
                                        <div class="col-6 d-flex align-items-center px-0">
                                            <img src="img/locker.svg">
                                            <span class="convenience-description" style="width: 50px">{{ __('rooms.loker') }}</span>
                                        </div>
                                    </div>
                                @endif
                            @endif
                            <div class="row">
                                <div class="col-md-8 text-center-mob">
                                    <span class="price-hotel">
                                        <strong>{{ $room->price }}</strong> {{ __('rooms.cost') }}*
                                    </span>
                                </div>
                                <p class="pl-3 mb-0" style="font-size: .6rem">* {{ __('rooms.change') }}
                                    <br/>{{ __('rooms.check-in') }} 14:00{{ __('rooms.pm') }}; {{ __('rooms.check-out') }} 11:00{{ __('rooms.am') }}</p>
                            </div>
                        </div>
                        <div class="col-lg-1 d-flex align-items-center justify-content-center">
                            <div class="row">
                                <div class="left-arrow-circle text-center next-slide-hotel">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="container-fluid services-container" id="services">
    <div class="row container-header">
        <div class="col-12 text-center">
            <h2>{{ __('services.title') }}</h2>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 serv-elem-wrapper d-flex align-items-center justify-content-center flex-column">
                <img src="{{ asset('img/washer.png') }}" alt="{{ __('services.wash') }}">
                <h6>{{ __('services.wash') }}</h6>
            </div>
            <div class="col-lg-3 serv-elem-wrapper d-flex align-items-center justify-content-center flex-column">
                <img src="{{ asset('img/bannie_pr.png') }}" alt="{{ __('services.bath') }}">
                <h6>{{ __('services.bath') }}</h6>
            </div>
            <div class="col-lg-3 serv-elem-wrapper d-flex align-items-center justify-content-center flex-column">
                <img src="{{ asset('img/excursion.png') }}" alt="{{ __('services.excursions') }}">
                <h6>{{ __('services.excursions') }}</h6>
            </div>
            <div class="col-lg-3 serv-elem-wrapper d-flex align-items-center justify-content-center flex-column">
                <img src="{{ asset('img/coffee.png') }}" alt="{{ __('services.coffee') }}">
                <h6>{{ __('services.coffee') }}</h6>
            </div>
        </div>
    </div>
</div>

{{--<div class="container-fluid reviews-container">
    <div class="row container-header">
        <div class="col-12 text-center">
            <h2 id="reviews">{{ __('reviews.title') }}</h2>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-1 left-arrow d-flex align-items-center justify-content-end">
                <div class="left-arrow-circle text-center" id="prev-review">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
            </div>
            <div class="col-10">
                <div class="row" id="comments-slider">
                    @foreach($comments as $comment)
                        <div>
                            <div class="col-lg-6 review-element">
                                <div class="review-wrapper">
                                    <div class="row">
                                        <div class="col-12 review-header d-flex align-items-center justify-content-between">
                                    <span class="username">
                                        {{ $comment->name }}
                                    </span>
                                            <span class="postdate">
                                        {{ date_format(date_create($comment->date), 'd.m.Y' )}}
                                    </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 stars-wrapper">
                                            @for($i = 0; $i < $comment->stars; $i++)
                                                <div class="star">
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                </div>
                                            @endfor
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 review-paragraph">
                                            <p>{{ $comment->message }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-1 right-arrow d-flex align-items-center justify-content-start">
                <div class="left-arrow-circle text-center" id="next-review">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</div>--}}

<div class="container-fluid mb-5">
    <div class="row">
        <div class="col-12 text-center">
            <h3><strong style="color: #00a025">Station Hostel</strong>{{ __('about.motto') }}</h3>
        </div>
    </div>
</div>

<div class="container-fluid contacts-container">
    <div class="row container-header">
        <div class="col-12 text-center">
            <h2>{{ __('contacts.title') }}</h2>
        </div>
    </div>
    <div class="container" id="contacts">
        <div class="row d-flex align-items-center justify-content-center" style="min-height: 75vh;">
            <div class="col-lg-5 offset-lg-1 map-wrapper">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1282.6490300217386!2d36.2058962661162!3d49.98702110034274!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4127a1aa000fee89%3A0xeb4d0b43367a762c!2sSTATION+HOSTEL!5e0!3m2!1sru!2sua!4v1533635038886"
                        width="100%" height="430" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="col-lg-5">
                <ul class="contacts-info-list">
                    <li><img src="img/location.png">{{ __('contacts.address') }}</li>
                    <li><a href="tel:+380688650047"><img src="img/phone.svg">+38 (068) 865 00 47</a></li>
                    <li><a href="tel:+380668650047"><img src="img/phone.svg">+38 (066) 865 00 47</a></li>
                    <li><a href="tel:+380738650047"><img src="img/phone.svg">+38 (073) 865 00 47</a></li>
                    <li><a href="mailto:administrator@station-hostel.com.ua"><img src="img/message-gr.png">administrator@station-hostel.com.ua</a>
                    </li>
                </ul>
                <div class="contact-form">
                    <h5 class="form-header text-center">{{ __('contacts.consultation') }}</h5>
                    <form class="text-center" action="/" method="post" id="feedback">
                        <div class="form-group row">
                            <label for="name"
                                   class="col-sm-2 col-form-label text-left">{{ __('contacts.name') }}</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone"
                                   class="col-sm-3 col-form-label text-left">{{ __('contacts.phone') }}</label>
                            <div class="col-sm-9 pl-0">
                                <input class="form-control" type="tel" id="phone" name="phone" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-sm-12 text-left no-border">{{ __('contacts.message') }}</label>
                            <div class="col-sm-12">
                                <textarea class="form-control" rows="1" name="text" id="text" required></textarea>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="main-button align-self-center" id="load">
                                <span>{{ __('contacts.submit') }}</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/datepicker.min.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/slick.min.js') }}"></script>
<script src="{{ asset('js/lightslider.js') }}"></script>
<script>
    $.fn.datepicker.language['en'] = {
        days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        today: 'Today',
        clear: 'Clear',
        dateFormat: 'dd.mm.yyyy',
        timeFormat: 'hh:ii aa',
        firstDay: 0
    };
    $('#from').datepicker({
        language: '{{ app()->getLocale() }}'
    });
    $('#to').datepicker({
        language: '{{ app()->getLocale() }}'
    });
</script>

<script src="{{ asset('js/slider.js') }}"></script>
<script src="{{ asset('js/scroll.js') }}"></script>
</body>
</html>