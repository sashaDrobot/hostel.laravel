@component('mail::message')
    # Забронирован номер

    Контактные данные:

    Имя: {{ $booking->name }}
    Email: {{ $booking->email }}
    Номер телефона: {{ $booking->phone }}

@endcomponent