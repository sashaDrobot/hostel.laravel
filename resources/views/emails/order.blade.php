@component('mail::message')
    # Новое сообщение

    Новое сообщение от клиента!

    Контактные данные:

    Имя: {{ $order->name }}
    Телефон: {{ $order->phone }}
    Сообщение: {{ $order->text }}

@endcomponent