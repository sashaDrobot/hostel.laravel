<?php

return [

    'title' => 'Номера и цены',
    'room' => 'Номер',
    'bedclothes' => 'чистое постельное белье',
    'mattresses' => 'ортопедические матрасы',
    'bedside ' => 'тумбочки',
    'furniture' => 'мебель',
    'towel' => 'полотенце',
    'satellite' => 'спутниковое телевидение',
    'power' => 'розетка',
    'lamp' => 'лампа у кровати',
    'stand' => 'подставка для телефона',
    'loker' => 'локер (шкафчик)',
    'cost' => 'грн/сутки',
    'change' => '- в выходные и праздничные дни цена может меняться',
    'check-in' => 'время заезда',
    'check-out' => 'время выезда',
    'pm' => '',
    'am' => ''

];