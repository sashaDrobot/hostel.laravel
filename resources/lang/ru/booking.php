<?php

return [

    'name' => 'Имя',
    'email' => 'Email',
    'phone' => 'Номер телефона',
    'city' => 'Город',
    'kha' => 'Харьков',
    'arrival-date' => 'Дата заезда',
    'date-departure' => 'Дата выезда',
    'guests' => 'Кол-во гостей',
    'booking' => 'Забронировать',

];