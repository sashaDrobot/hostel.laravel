<?php

return [

    'home' => 'Главная',
    'about' => 'О нас',
    'advantages' => 'Преимущества',
    'prices' => 'Номера и цены',
    'services' => 'Доп. услуги',
    'reviews' => 'Отзывы',
    'contacts' => 'Контакты',
    'callback' => 'Связаться с нами'

];