<?php

return [

    'title' => 'Contacts',
    'address' => 'Ukraine, Kharkov str. Poltavskiy shlyah, 60',
    'consultation' => 'Get in touch',
    'name' => 'Name:',
    'phone' => 'Phone:',
    'message' => 'Comments:',
    'submit' => 'Send',

    'success' => 'Success!',

];