<?php

return [

    'name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'city' => 'City',
    'kha' => 'Kharkiv',
    'arrival-date' => 'Check-in',
    'date-departure' => 'Check-out',
    'guests' => 'Guests',
    'booking' => 'Book Now',

];