<?php

return [

    'about' => ' - cozy and modern hostel, located within two minutes walk from Train Station. Being a guest in our lovely city you will be able to rest in comfort and calmness. Welcome to enjoy the cosmopolitan atmosphere in our lounge area, stocked high-tech kitchen and luggage storage are at your complete disposal. Our prices will surprise you, come and check on your own!',

    'motto' => ' - feel like at home'

];
