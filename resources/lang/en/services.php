<?php

return [

    'title' => 'Services',
    'wash' => 'Laundry',
    'bath' => 'Bathroom stuff',
    'excursions' => 'City tours',
    'coffee' => 'Perfect beans',

];