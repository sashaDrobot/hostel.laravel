<?php

return [

    'home' => 'Home',
    'about' => 'About us',
    'advantages' => 'Benefits',
    'prices' => 'Rooms & prices',
    'services' => 'Services',
    'reviews' => 'Reviews',
    'contacts' => 'Contacts',
    'callback' => 'Get in touch'

];