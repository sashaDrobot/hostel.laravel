<?php

return [

    'title' => 'Benefits of the ',
    'two-min' => 'Two minutes walk to ',
    'south-station' => 'Train Station',
    'center' => 'Located in the city',
    'city' => 'center',
    'hostel' => 'Premium class hostel',
    'premium' => '',
    'friendly' => 'Friendly & attentive',
    'staff' => 'staff',
    'bathroom' => 'Spacious bathroom',
    'satellite' => 'SAT TV',
    'kitchen' => 'High-tech equipped kitchen',

];