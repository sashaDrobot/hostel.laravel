<?php

return [

    'title' => 'Rooms & prices',
    'room' => 'Room',
    'bedclothes' => 'clean bedding',
    'mattresses' => 'orthopedic mattresses',
    'bedside ' => 'night table',
    'furniture' => 'furniture',
    'towel' => 'towel',
    'satellite' => 'satellite TV',
    'power' => 'socket',
    'lamp' => 'private lightening',
    'stand' => 'shelf',
    'loker' => 'loker',
    'cost' => 'UAH per night',
    'change' => '- during holidays and weekends prices can change',
    'check-in' => 'Check-in at',
    'check-out' => 'Check-out at',
    'pm' => ' PM',
    'am' => ' AM'

];