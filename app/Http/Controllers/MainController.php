<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Comment;
use App\Mail\SendOrder;
use App\Mail\SendBooking;
use App\Order;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
    public function index()
    {
        $rooms = Room::get();
        return view('index', ['rooms' => $rooms]);
    }

    public function test()
    {
        $rooms = Room::get();
        return view('test', ['rooms' => $rooms]);
    }

    public function booking(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string:max:150',
            'email' => 'sometimes|nullable|string|email|max:100',
            'phone' => 'required|string|max:20',
        //    'arrival' => 'required|max:20|date|after:yesterday',
        //    'departure' => 'required|max:20|date|after:arrival',
        //    'guests' => 'required|digits:1',
        ]);

        $booking = new Booking();

        $booking->name = $request->name;
        $booking->email = $request->email ?? '';
        $booking->phone = $request->phone;
 //       $booking->arrival_date = $request->arrival;
   //     $booking->departure_date = $request->departure;
     //   $booking->guest = $request->guests;

        $booking->save();

        Mail::to('alex.drobot22@gmail.com')
            ->send(new SendBooking($booking));

        $response = Lang::get('contacts.success');

        return response()->json(array('msg' => $response), 200);
        /*

        $arrival = date_format(date_create($request->arrival), 'Y-m-d');
        $departure = date_create($request->departure);
        $guests = $request->guests;

        $nights = date_diff(date_create($arrival), $departure)->format('%a');

        $to = "https://booking.frontdeskmaster.com/?hostelId=3tHDNpA9q9fw%2FyNTgP6SfI5ypEmSn%2BD%2B#nights=$nights&guests=$guests&arrival=$arrival";

         if ($request->ajax()) {
             return $to;
         }

         return redirect($to);*/
    }

    public function wait()
    {
        return 'Please wait...';
    }

    public function feedback(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'phone' => 'required|max:20',
            'text' => 'required|max:500'
        ]);

        $order = new Order;

        $order->name = $request->name;
        $order->phone = $request->phone;
        $order->text = $request->text;

        $order->save();

        Mail::to('administrator@station-hostel.com.ua')
            ->send(new SendOrder($order));

        $response = Lang::get('contacts.success');

        return response()->json(array('msg' => $response), 200);
    }
}
