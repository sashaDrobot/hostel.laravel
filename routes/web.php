<?php

Route::get('/', 'MainController@index');
Route::post('/', 'MainController@feedback');

Route::get('/setlocale/{locale}', 'LocalizationController@setLocale');

Route::get('/booking', 'MainController@booking');
Route::get('/wait', 'MainController@wait');

Route::get('/mailable', function () {
    $order = App\Order::find(1);
    return new App\Mail\SendOrder($order);
});